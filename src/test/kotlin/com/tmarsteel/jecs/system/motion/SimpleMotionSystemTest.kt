package com.tmarsteel.jecs.system.motion

import com.tmarsteel.jecs.BaseEntity
import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.system.motion.collision.DefaultCollidingComponent
import com.tmarsteel.jecs.util.Vector
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

/**
 * @author Tobias Marstaller
 */
class SimpleMotionSystemTest {
    private var entitySystem: EntitySystem? = null
    private var motionSystem: SimpleMotionSystem? = null
    private var entity: Entity? = null
    private var velocityComponent: VelocityComponent? = null
    private var locationComponent: LocationComponent? = null

    @Before
    fun setUp() {
        // component under test: HealthSystem
        motionSystem = SimpleMotionSystem()

        // Mocking these is more complex than compensating the invisible test-dependency in case of an error
        velocityComponent = VelocityComponent()
        locationComponent = LocationComponent(Vector.ORIGIN)

        entitySystem = EntitySystem()
        entitySystem!!.register(motionSystem!!)

        // mock the entity
        entity = object: BaseEntity(entitySystem!!) {}
        entitySystem!!.register(entity!!)

        // mock the entity system
        entity!!.addComponent(velocityComponent!!)
        entity!!.addComponent(locationComponent!!)
    }

    @Test
    fun testSimpleMove() {
        // SETUP
        velocityComponent!!.velocity = Vector(5.0, 0.0, 1.0)

        // ACT
        motionSystem!!.tick(entitySystem!!, .1f)

        // VERIFY
        assertEquals(0.5, locationComponent!!.position.x, 1e-5)
        assertEquals(0.0, locationComponent!!.position.y, 1e-5)
        assertEquals(0.1, locationComponent!!.position.z, 1e-5)
    }

    @Test
    fun testMovementListenerIsCalled() {
        // SETUP
        velocityComponent!!.velocity = Vector(5.0, 0.0, 1.0)

        var called: Boolean = false
        motionSystem!!.registerEntityMovementListener({ called = true })

        // ACT
        motionSystem!!.tick(entitySystem!!, .1f)

        // VERIFY
        assertTrue("EntityMotionListener was not called!", called)
    }

    @Test
    fun testCollisionAgainstStaticEntity() {
        // SETUP
        velocityComponent!!.velocity = Vector(1.0, 0.0, 0.0)
        locationComponent!!.position = Vector.ORIGIN

        val staticEntity = object : BaseEntity(entitySystem!!) {}
        val staticEntityLocationC = LocationComponent()

        val movingEntityCollidingC: CollidingComponent = object : DefaultCollidingComponent(PhysicalStateReferenceProvider.Companion.of(locationComponent!!)) {
            override fun collidesWith(otherComponent: CollidingComponent): Boolean {
                return true
            }
        }
        val staticEntityCollidingC = object : DefaultCollidingComponent(PhysicalStateReferenceProvider.Companion.of(locationComponent!!)) {
            override fun collidesWith(otherComponent: CollidingComponent): Boolean {
                return true
            }
        }

        entity!!.addComponent(movingEntityCollidingC, CollidingComponent::class.java as Class<in Component>)
        staticEntity.addComponent(staticEntityLocationC)
        staticEntity.addComponent(staticEntityCollidingC, CollidingComponent::class.java as Class<in Component>)

        motionSystem!!.tick(entitySystem!!, 1.toFloat())

        assertEquals("Entity moved even though it collides with another static entity", locationComponent!!.position, Vector.ORIGIN)
    }

    @Test
    fun testCollisionAgainstIdleEntity() {
        // SETUP
        velocityComponent!!.velocity = Vector(1.0, 0.0, 0.0)
        locationComponent!!.position = Vector.ORIGIN

        val idleEntity = object : BaseEntity(entitySystem!!) {}
        val idleEntityLocationC = LocationComponent()
        val idleEntityVelocityC = VelocityComponent()

        val movingEntityCollidingC: CollidingComponent = object : DefaultCollidingComponent(PhysicalStateReferenceProvider.Companion.of(locationComponent!!)) {
            override fun collidesWith(otherComponent: CollidingComponent): Boolean {
                return true
            }
        }
        val idleEntityCollidingC = object : DefaultCollidingComponent(PhysicalStateReferenceProvider.Companion.of(idleEntityLocationC!!)) {
            override fun collidesWith(otherComponent: CollidingComponent): Boolean {
                return true
            }
        }

        entity!!.addComponent(movingEntityCollidingC, CollidingComponent::class.java as Class<in Component>)
        idleEntity.addComponent(idleEntityCollidingC);
        idleEntity.addComponent(idleEntityVelocityC);
        idleEntity.addComponent(idleEntityCollidingC, CollidingComponent::class.java as Class<in Component>)

        // ACT
        motionSystem!!.tick(entitySystem!!, 1.toFloat())

        // ASSERT
        assertEquals("Entity moved even though it collides with another idle entity", locationComponent!!.position, Vector.ORIGIN)
    }

    @Test
    fun testCollisionOfTwoMovingEntities() {
        // SETUP
        velocityComponent!!.velocity = Vector(1.0, 0.0, 0.0)
        locationComponent!!.position = Vector.ORIGIN

        val otherEntity = object: BaseEntity(entitySystem!!) {}
        val otherEntityLocationC = LocationComponent()
        val otherEntityVelocityC = VelocityComponent(Vector(1.0, 0.0, 0.0))

        val controlEntity = object: BaseEntity(entitySystem!!) {}
        val controlEntityLocationC = LocationComponent()
        val controlEntityVelocityC = VelocityComponent(Vector(1.0, 0.0, 0.0))

        val movingEntityCollidingC: CollidingComponent = object : DefaultCollidingComponent(PhysicalStateReferenceProvider.Companion.of(locationComponent!!)) {
            override fun collidesWith(otherComponent: CollidingComponent): Boolean {
                return true
            }
        }
        val otherEntityCollidingC = object : DefaultCollidingComponent(PhysicalStateReferenceProvider.Companion.of(otherEntityLocationC!!)) {
            override fun collidesWith(otherComponent: CollidingComponent): Boolean {
                return true
            }
        }

        entity!!.addComponent(movingEntityCollidingC, CollidingComponent::class.java as Class<in Component>)
        otherEntity.addComponent(otherEntityCollidingC);
        otherEntity.addComponent(otherEntityVelocityC);
        otherEntity.addComponent(otherEntityCollidingC, CollidingComponent::class.java as Class<in Component>)
        controlEntity.addComponent(controlEntityLocationC)
        controlEntity.addComponent(controlEntityVelocityC)

        // ACT
        motionSystem!!.tick(entitySystem!!, 1.toFloat())

        // ASSERT
        assertEquals("Entity moved even though it collides with another moving entity", locationComponent!!.position, Vector.ORIGIN)
        assertEquals("Entity moved even though it collides with another moving entity", otherEntityLocationC.position, Vector.ORIGIN)
        assertEquals("Entity did not move when it should have", controlEntityLocationC.position, Vector(1.0, 0.0, 0.0))
    }
}