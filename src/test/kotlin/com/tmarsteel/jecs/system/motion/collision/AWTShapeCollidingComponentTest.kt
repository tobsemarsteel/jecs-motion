package com.tmarsteel.jecs.system.motion.collision

import com.tmarsteel.jecs.system.motion.CollidingComponent
import com.tmarsteel.jecs.system.motion.PhysicalStateReferenceProvider
import com.tmarsteel.jecs.util.Vector
import org.junit.Test

import org.junit.Assert.*
import java.awt.Rectangle

/**
 * @author Tobias Marstaller @gmail.com>
 */
class AWTShapeCollidingComponentTest {

    val originReferencePointProvider = object: PhysicalStateReferenceProvider() {
        override val location = Vector.ORIGIN
        override val rotation = Vector.ORIGIN
    }

    @Test
    fun testSelfCollisionAvoidance() {
        // SETUP
        val shape = Rectangle(-2, -2, 4, 4)
        val component = AWTShapeCollidingComponent(originReferencePointProvider, shape)

        // ACT
        val result = component.collidesWith(component)

        // ASSERT
        assertFalse("CollidingComponent must not collide with itself but does", result)
    }

    @Test
    fun testLayerCheck() {
        // SETUP
        val shape1 = Rectangle(-2, -2, 4, 4)
        val shape2 = Rectangle(0, 0,   4, 4)
        val component1 = AWTShapeCollidingComponent(originReferencePointProvider, shape1)
        val component2 = AWTShapeCollidingComponent(originReferencePointProvider, shape2)
        component1.activateLayers(1)
        component2.activateLayers(2)
        // the shapes intersect; but since the components do not share a layer, they should not collide

        // ACT
        val result = component1.collidesWith(component2)

        // ASSERT
        assertFalse("CollidingComponent reports a collision for colliding shapes without a common layer", result)
    }

    @Test
    fun testSimpleCollision() {
        // SETUP
        val shape1 = Rectangle(-2, -2, 4, 4)
        val shape2 = Rectangle(0, 0,   4, 4)
        val component1 = AWTShapeCollidingComponent(originReferencePointProvider, shape1)
        val component2 = AWTShapeCollidingComponent(originReferencePointProvider, shape2)
        component1.activateLayers(1)
        component2.activateLayers(1)

        // ACT
        var result = component1.collidesWith(component2)

        // ASSERT
        assertTrue("CollidingComponent reports no collision for colliding shapes with common layer", result)
    }

    @Test
    fun testWithTranslation_NoCollision() {
        // SETUP
        val shape1 = Rectangle(-2, -2, 4, 4)
        val shape2 = Rectangle(0, 0,   4, 4)

        val component2ReferenceProvider = object: PhysicalStateReferenceProvider() {
            override val location = Vector(3.0, 3.0, 0.0)
            override val rotation = Vector.ORIGIN
        }

        val component1 = AWTShapeCollidingComponent(originReferencePointProvider, shape1)
        val component2 = AWTShapeCollidingComponent(component2ReferenceProvider, shape2)
        component1.activateLayers(1)
        component2.activateLayers(1)

        // ACT
        var result = component1.collidesWith(component2)

        // ASSERT
        assertFalse("CollidingComponent reports collision for non colliding shapes with common layer", result)
    }

    @Test
    fun testWithTranslation_WithCollision() {
        // SETUP
        val shape1 = Rectangle(0, 0, 4, 4)
        val shape2 = Rectangle(-4, -5, 4, 4)

        val component2ReferenceProvider = object: PhysicalStateReferenceProvider() {
            override val location = Vector(3.0, 4.0, 0.0)
            override val rotation = Vector.ORIGIN
        }

        val component1 = AWTShapeCollidingComponent(originReferencePointProvider, shape1)
        val component2 = AWTShapeCollidingComponent(component2ReferenceProvider, shape2)
        component1.activateLayers(1)
        component2.activateLayers(1)

        // ACT
        var result = component1.collidesWith(component2)

        // ASSERT
        assertTrue("CollidingComponent reports collision for non colliding shapes with common layer", result)
    }

    @Test
    fun testWithRotation_NoCollision() {
        // SETUP
        val shape1 = Rectangle(80, 0, 40, 40)
        val shape2 = Rectangle(0,  0, 40, 40)

        val component2ReferenceProvider = object: PhysicalStateReferenceProvider() {
            override val location = Vector(50.0, 35.0, 0.0)
            override val rotation = Vector(0.0, 0.0, Math.PI / 4)
        }

        val component1 = AWTShapeCollidingComponent(originReferencePointProvider, shape1)
        val component2 = AWTShapeCollidingComponent(component2ReferenceProvider, shape2)
        component1.activateLayers(1)
        component2.activateLayers(1)

        // ACT
        var result = component1.collidesWith(component2)

        // ASSERT
        assertFalse("CollidingComponent reports collision for non colliding shapes with common layer", result)
    }

    @Test
    fun testWithRotation_WithCollision() {
        // SETUP
        val shape1 = Rectangle(80, 0, 40, 40)
        val shape2 = Rectangle(0,  0, 40, 40)

        val component2ReferenceProvider = object: PhysicalStateReferenceProvider() {
            override val location = Vector(65.0, 0.0, 0.0)
            override val rotation = Vector(0.0, 0.0, Math.PI / 4)
        }

        val component1 = AWTShapeCollidingComponent(originReferencePointProvider, shape1)
        val component2 = AWTShapeCollidingComponent(component2ReferenceProvider, shape2)
        component1.activateLayers(1)
        component2.activateLayers(1)

        // ACT
        var result = component1.collidesWith(component2)

        // ASSERT
        assertTrue("CollidingComponent reports no collision for colliding shapes with common layer", result)
    }
}
