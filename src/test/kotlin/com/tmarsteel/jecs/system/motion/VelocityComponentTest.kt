package com.tmarsteel.jecs.system.motion

import com.tmarsteel.jecs.util.Vector
import org.junit.Test

import junit.framework.TestCase.assertEquals

/**
 * @author Tobias Marstaller @gmail.com>
 */
class VelocityComponentTest {
    @Test
    fun defaultComponentShouldReportZeroVelocity() {
        val c = VelocityComponent()

        assertEquals("Initial speed != 0", 0.0, c.velocity.length)
    }

    @Test
    fun testAdjustedVelocity() {
        // PREPARE
        val c = VelocityComponent()

        val velocity = Vector(2.0, 2.0, 2.0)

        c.velocity = velocity

        // ACT + ASSERT
        assertEquals(velocity.multiply(2.0), c.adjustedVelocity(2f))
    }
}
