package com.tmarsteel.jecs.system.motion;

import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.util.Vector;

/**
 * Adds rotation to [Entity]s.
 * @author Tobias Marstaller
 */
open class RotationComponent : Component {
    override val type = RotationComponent::class.java

    /**
     * The rotation stored in this component. Each component of this vector represents the rotation
     * around the respective axis in radians. For example: the vector (PI, 0, 0.5 * PI) describes
     * 180 degrees rotation round the X axis, no Y rotation and 90 degrees rotation around the Z axis.
     */
    protected var rotationVector = Vector.ORIGIN

    /**
     * Rotation around the X axis in radians
     */
    var x: Double
        get() = rotationVector.x
        set(value) {
            rotationVector = rotationVector.add(Vector(value, 0.0, 0.0))
        }

    /**
     * Rotation around the Y axis in radians
     */
    var y: Double
        get() = rotationVector.y
        set(value) {
            rotationVector = rotationVector.add(Vector(0.0, value, 0.0))
        }

    /**
     * Rotation around the Z axis in radians
     */
    var z: Double
        get() = rotationVector.z
        set(value) {
            rotationVector = rotationVector.add(Vector(0.0, 0.0, value))
        }

    /**
     * Returns all three rotation values as a vector. Each component of this vector represents the
     * rotation around the respective axis in radians. For example: the vector (PI, 0, 0.5 * PI)
     * describes 180 degrees rotation round the X axis, no Y rotation and 90 degrees rotation around
     * the Z axis.
     */
    fun asVector(): Vector = rotationVector
}
