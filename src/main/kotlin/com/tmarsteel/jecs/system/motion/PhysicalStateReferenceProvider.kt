package com.tmarsteel.jecs.system.motion

import com.tmarsteel.jecs.util.Vector

/**
 * This class assits in hitbox calculation. This is required because of the following situation: As an entity moves and
 * rotates through space, the hitbox must adapt. This is achieved by re-calculating the hitbox based on the current
 * position and rotation of the associated entity. [PhysicalStateReferenceProvider]s are a shortcut for code working
 * with hitboxes as such codes does not have to query the [EntitySystem] for [LocationComponent]s and [RotationComponent]s
 * every tick.
 *
 * **Important:** a hitbox' reference point should aways be the center of the shape. Adjusting the reference point
 * so that the hitbox makes the best-possible match with the rendered 2D graphics/3D model is a concern of the entity
 * or the [PhysicalStateReferenceProvider]; collision detection in [SimpleMotionSystem]s does not account for such
 * adjustments.
 *
 * **Note on subclassing:** Instances of this class working nicely with [LocationComponent] and [RotationComponent]
 * can be created using the [of] methods. If you need any special functionality (such as accounting for a shift
 * in entity location and hitbox location), you need to subclass.
 * @author Tobias Marstaller
 */
abstract class PhysicalStateReferenceProvider
{

    companion object
    {
        /**
         * Creates and returns a [PhysicalStateReferenceProvider] that delegates to the given [LocationComponent] and
         * [RotationComponent].
         */
        fun of(locationC : LocationComponent, rotationC : RotationComponent) = object : PhysicalStateReferenceProvider() {
            override val location = locationC.position
            override val rotation = rotationC.asVector()
        }

        /**
         * Creates and returns a [PhysicalStateReferenceProvider] that delegates to the given [LocationComponent] and
         * reports no rotation (see [Vector.ORIGIN]).
         */
        fun of(locationC : LocationComponent) = object : PhysicalStateReferenceProvider() {
            override val location = locationC.position
            override val rotation = Vector.ORIGIN
        }
    }

    /**
     * The adjusted reference point relative to the associated entities location in space.
     */
    abstract val location : Vector

    /**
     * The rotation state of the entity.
     * @see RotationComponent.asVector
     */
    abstract val rotation : Vector
}
