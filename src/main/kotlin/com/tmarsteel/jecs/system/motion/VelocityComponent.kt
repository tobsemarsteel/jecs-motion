package com.tmarsteel.jecs.system.motion

import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.util.Vector

/**
 * This component is used by the [SimpleMotionSystem]. It allows entities to move.
 * @author Tobias Marstaller
 */
open class VelocityComponent(var velocity: Vector = Vector.ORIGIN) : Component {

    override val type = VelocityComponent::class.java

    /**
     * The value of the parameter [.adjustedVelocity] was last called with. If it differs from the given
     * parameter the vector will be calculated again; otherwise [.lastAdjustedVelocity] will be returned.
     * things up and safe memory.
     */
    private var lastAdjustedVelocityFactor: Float = 0.toFloat()

    /**
     * Cache for [.adjustedVelocity]. It is expected that the [com.tmarsteel.jecs.EntitySystem] and
     * thus the motion systems will always calculate with the same tick duration. Caching the result will speed
     * things up and safe memory.
     */
    private var lastAdjustedVelocity: Vector = velocity

    /**
     * The object this component locks on when the adjusted velocity has to be re-calculated
     */
    private val adjustedLock = Object()

    /**
     * Returns the current velocity of this component multiplied by `factor`. Effectively returns
     * `tellVelocity().multiply(factor)`. However, the result is cached since it is expected that, because
     * the tick duration is stable most of the time, `factor` changes rarely.
     * @param factor
     * @return The current velocity of this component multiplied by `factor`.
     */
    fun adjustedVelocity(factor: Float): Vector {
        if (lastAdjustedVelocityFactor != factor) {
            synchronized (adjustedLock) {
                if (lastAdjustedVelocityFactor != factor) {
                    lastAdjustedVelocityFactor = factor
                    lastAdjustedVelocity = velocity.multiply(factor.toDouble())
                }
            }
        }

        return lastAdjustedVelocity
    }
}