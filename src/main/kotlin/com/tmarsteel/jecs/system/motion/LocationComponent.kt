package com.tmarsteel.jecs.system.motion;

import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.util.Vector

/**
 * Adds a physical location to [Entity]s.
 * @author Tobias Marstaller
 */
open class LocationComponent
    /**
     * @param position The initial position of this component
     */
    (var position: Vector = Vector.ORIGIN) : Component {

    override val type = LocationComponent::class.java

    /**
     * Adds the given vector to the position of this component
     */
    fun shiftBy(distance: Vector) {
        position = position.add(distance)
    }

    /**
     * @return [position]
     */
    fun invoke() : Vector = position
}

