package com.tmarsteel.jecs.system.motion.collision

import com.tmarsteel.jecs.system.motion.CollidingComponent
import com.tmarsteel.jecs.system.motion.PhysicalStateReferenceProvider

open abstract class DefaultCollidingComponent(protected var physicalStateReferenceProvider: PhysicalStateReferenceProvider) : CollidingComponent {
    override var activeLayers: Long = 0
    override val type = CollidingComponent::class.java
}