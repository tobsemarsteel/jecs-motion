package com.tmarsteel.jecs.system.motion.collision

import com.tmarsteel.jecs.system.motion.CollidingComponent
import com.tmarsteel.jecs.system.motion.PhysicalStateReferenceProvider
import com.tmarsteel.jecs.util.Vector
import java.awt.Color
import java.awt.Rectangle
import java.awt.Shape
import java.awt.geom.AffineTransform
import java.awt.geom.Area
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import javax.naming.OperationNotSupportedException

/**
 * A 2D AWT [Shape] colliding component.
 *
 * **Important:** This is a 2-dimensional component. It entirely ignores the Z-axis position as well as the
 * X- and Y-axis rotation.
 */
class AWTShapeCollidingComponent

/**
 * @param shape The shape to use for collision detection; the center of the shape should be at (0|0)
 */
constructor(rpp: PhysicalStateReferenceProvider, shape: Shape) : DefaultCollidingComponent(rpp) {

    private val area: Area = Area(shape)

    override fun collidesWith(otherComponent: CollidingComponent): Boolean {
        if (otherComponent === this) return false
        if (otherComponent.activeLayers and activeLayers == 0L) return false

        if (otherComponent !is AWTShapeCollidingComponent) {
            throw OperationNotSupportedException("The given other component is not an instance of ${this.javaClass.name}")
        }

        // rotation
        val thisRot = physicalStateReferenceProvider.rotation.z
        val otherRot = otherComponent.physicalStateReferenceProvider.rotation.z

        var thisArea : Area = if (thisRot == 0.0) this.area.clone() as Area else {
            this.area.createTransformedArea(AffineTransform.getRotateInstance(thisRot))
        }
        var otherArea : Area = if (otherRot == 0.0) otherComponent.area.clone() as Area else {
            otherComponent.area.createTransformedArea(AffineTransform.getRotateInstance(otherRot))
        }

        // translation
        val thisRP = physicalStateReferenceProvider.location
        val otherRP = otherComponent.physicalStateReferenceProvider.location

        if (thisRP.x != 0.0 || thisRP.y != 0.0) {
            thisArea = thisArea.createTransformedArea(AffineTransform.getTranslateInstance(thisRP.x, thisRP.y))
        }
        if (otherRP.x != 0.0 || otherRP.y != 0.0) {
            otherArea = otherArea.createTransformedArea(AffineTransform.getTranslateInstance(otherRP.x, otherRP.y))
        }

        // check for collision
        thisArea.intersect(otherArea)
        return !thisArea.isEmpty
    }

    /**
     * Debugging utility: Draws the two given areas to an image and stores it in a file.
     */
    private fun debugAreas(area1: Area, area2: Area, filename: String) {
        val image = BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB)
        val g2d = image.createGraphics()
        g2d.color = Color.WHITE
        g2d.fill(Rectangle(200, 200))
        g2d.color = Color.BLUE
        g2d.fill(area1)
        g2d.color = Color.RED
        g2d.fill(area2)

        val interset = (area1.clone() as Area)
        interset.intersect(area2)

        if (!interset.isEmpty) {
            g2d.color = Color.ORANGE
            g2d.fill(interset)
        }

        g2d.dispose()

        ImageIO.write(image, "JPEG", File(filename))
    }
}