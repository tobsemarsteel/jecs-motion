package com.tmarsteel.jecs.system.motion

import com.tmarsteel.jecs.component.Component

/**
 * This component allows entities to collide with other entities equipped with a [CollidingComponent], too.
 * It is assumed that the there is only one subclass of [CollidingComponent] in use in an [EntitySystem].
 * Collision is spread to layers: An entity has to be registered for layers. Once registered it can collide with
 * entities registered for the same layers. The layers are represented as bit flags in a 64 bit integer (`long`).
 *
 * Creating constants that define the layer flags make your code a lot more readable. For example:
 *
 *     player.getComponent(CollidingComponent.class).activateLayers(
 *         GameConstants.COLLISION_LAYER_PLAYERS | GameConstants.COLLISION_LAYER_MAPOBJECTS
 *     );
 *     // versus
 *     player.getComponent(CollidingComponent.class).activateLayers(0b0010001000);
 *
 * @author Tobias Marstaller
 */
interface CollidingComponent : Component {

    /**
     * The active layers as a 64 bit integer containing the flag values
     */
    var activeLayers: Long

    /**
     * Assures that the layers flagged in `layerMask` (bits set in `layerMask`) are activated.
     * @param layerMask The layers to activate, represented as set bits set in the 64 bit integer
     * @return The resulting, active layers (see [.activeLayers])
     */
    fun activateLayers(layerMask: Long) : Long
    {
        activeLayers = activeLayers or layerMask
        return activeLayers
    }

    /**
     * Assures that the layers flagged in `layerMask` (bits set in `layerMask`) are deactivated.
     * @param layerMask The layers to deactivate, represented as set bits in the 64 bit integer
     * @return The resulting, active layers (see [.activeLayers])
     */
    fun deactivateLayers(layerMask: Long) : Long
    {
        activeLayers = activeLayers xor (activeLayers and layerMask)
        return activeLayers
    }

    /**
     * Returns whether this component collides with the given other component. Immediately returns in these cases:
     * <table>
     *     <thead>
     *         <tr>
     *             <td>Condition</td>
     *             <td>Return value</td>
     *         </tr>
     *     </thead>
     *     <tbody>
     *         <tr>
     *             <td>this and otherComponent refer to the same object</td>
     *             <td><code>false</code></td>
     *         </tr>
     *         <tr>
     *             <td><code>No common collision layer</code></td>
     *             <td><code>false</code></td>
     *         </tr>
     *     </tbody>
     * </table>
     * @param otherComponent The component to check for a collision with
     * @return Whether this component and the given component collide.
     */
    fun collidesWith(otherComponent: CollidingComponent) : Boolean
}