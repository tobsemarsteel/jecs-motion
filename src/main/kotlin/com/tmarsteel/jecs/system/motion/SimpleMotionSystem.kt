package com.tmarsteel.jecs.system.motion

import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.system.ComponentSystem
import com.tmarsteel.jecs.util.Vector

import java.util.HashMap
import java.util.HashSet

/**
 * Updates the [LocationComponent] of an entity based on its [VelocityComponent]. Collisions will simply result in an
 * [Entity] inability to move further in its current direction.
 * @author Tobias Marstaller
 */
class SimpleMotionSystem : ComponentSystem {
    private val movementListeners = HashSet<(Entity) -> Unit>()

    /**
     * Assures the given listener is called each tick for all moving entities.
     * @param l The listener to register
     */
    fun registerEntityMovementListener(l: (Entity) -> Unit) {
        movementListeners.add(l)
    }

    /**
     * Assures the given listener is not called by this system anymore.
     * @param l The listener to unregister
     */
    fun unregisterEntityMovementListener(l: (Entity) -> Unit) {
        movementListeners.remove(l)
    }

    override fun tick(es: EntitySystem, tickDuration: Float) {
        val appliedMovements = HashMap<Entity, Vector>()

        // for non-colliding entities: move
        // for colliding entities: assume the movement; if later a collision is detected the movement is reverted
        es.each({ entity ->
                    val movedDistance = entity.getComponent(VelocityComponent::class.java).velocity.multiply(tickDuration.toDouble())

                    if (movedDistance.length > 0) {
                        entity.getComponent(LocationComponent::class.java).shiftBy(movedDistance)
                        appliedMovements.put(entity, movedDistance)
                    }
                },
                VelocityComponent::class.java,
                LocationComponent::class.java
        )

        // after the next forEach, this will hold the set of entities that collied and whichs movements have to be reverted
        val collidedEntities = HashSet<Entity>()

        // now, for the entities that move and can collide: check for collisions
        es.each({ entity ->
                val entityCollisionComponent = entity.getComponent(CollidingComponent::class.java)

                // check for collisions with all other collidable entities
                es.each({ otherEntity ->
                        if (otherEntity !== entity) { // nothing collides with itself
                            val otherEntityCollisionComponent = otherEntity.getComponent(CollidingComponent::class.java)

                            if (entityCollisionComponent.collidesWith(otherEntityCollisionComponent)) {
                                collidedEntities.add(entity)
                                // other entity does not move necessarily; if it does, the collision will be detected
                                // in another run of this loop
                            }
                        }
                    },
                    CollidingComponent::class.java
                )
            },
            VelocityComponent::class.java,
            LocationComponent::class.java,
            CollidingComponent::class.java
        )

        // collidedEntities now contains the set of entities that collide and cannot move
        // this forEach reverts the movement
        collidedEntities.forEach { entity ->
            if (appliedMovements.containsKey(entity)) {
                entity.getComponent(LocationComponent::class.java).shiftBy(
                        appliedMovements[entity]!!.inverse())
            }
        }

        // now, the entities from appliedMovements minus those from collidingEntities successfully moved
        // => trigger listeners
        val movedEntities = appliedMovements.keys
        movedEntities.removeAll(collidedEntities)
        movementListeners.forEach { l -> movedEntities.forEach { e -> l(e) } }
    }
}
